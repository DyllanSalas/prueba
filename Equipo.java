import javax.swing.JOptionPane;


public class Equipo 
{
	
	private String placa, marca, modelo, tipo, accesorios;
	
	private boolean estado;
	
	public Equipo(String placa, String marca, String modelo, String tipo, String accesorios, boolean estado)
	{
		
		setPlaca(placa);
		setMarca(marca);
		setModelo(modelo);
		setTipo(tipo);
		setAccesorios(accesorios);
		setEstado(estado);
		
	}
	
	//set y get placa
	
	public void setPlaca(String placa)
	{
		
		this.placa = placa;
		
	}
	
	public String getPlaca()
	{
		
		return placa;
		
	}
	
	//set y get marca
	
	public void setMarca(String marca)
	{
		
		this.marca = marca;
		
	}
	
	public String getMarca()
	{
		
		return marca;
		
	}
	
	//set y get modelo
	
	public void setModelo(String modelo)
	{
		
		this.modelo = modelo;
		
	}
	
	public String getModelo()
	{
		
		return modelo;
		
	}
	
	//set y get tipo
	
	public void setTipo(String tipo)
	{
		
		this.tipo = tipo;
		
	}
	
	public String getTipo()
	{
		
		return tipo;
		
	}
	
	//set y get accesorios
	
	public void setAccesorios(String accesorios)
	{
		
		this.accesorios = accesorios;
		
	}
	
	public String getAccesorios()
	{
		
		return accesorios;
		
	}
	
	//set y get estado
	
	public void setEstado(boolean estado)
	{
		
		this.estado = estado;
		
	}
	
	public boolean getEstado()
	{
		
		return estado;
		
	}
	
	public void getInformacion()
	{
		
		JOptionPane.showInputDialog(null,"Placa:"+getPlaca()+"\nMarca: "+getMarca()+"\nModelo: "+getModelo()+"\nTipo: "+getTipo()+"\nTipo: "+getTipo()+"\nAccesorios: "+getAccesorios());
		
	}
	
	public String toString()
	{
		
		return "Placa:"+getPlaca()+"\nMarca: "+getMarca()+"\nModelo: "+getModelo()+"\nTipo: "+getTipo()+"\nAccesorios: "+getAccesorios();
		
	}


}

